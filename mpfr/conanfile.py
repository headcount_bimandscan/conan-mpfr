#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import AutoToolsBuildEnvironment, \
                   tools

from conans.model.conan_file import ConanFile


# Based on: https://github.com/kheaactua/conan-mpfr/blob/4.0.1/conanfile.py
class MPFR(ConanFile):
    name = "mpfr"
    version = "4.0.1"
    license = "MIT"
    url = "https://bitbucket.org/headcount_bimandscan/conan-mpfr"
    description = "The GNU 'Multiple Precision Arithmetic Library'."
    generators = "cmake"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://www.mpfr.org/"

    _src_dir = f"{name}-{version}"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    options = {
                  "shared": [
                                True,
                                False
                            ],
                  "fPIC": [
                              True,
                              False
                          ]
              }

    default_options = "shared=False", \
                      "fPIC=True"

    exports = "../LICENCE.md"

    requires = "gmp/6.1.2@bincrafters/stable"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.settings.os == "Windows":
            self.options["gmp"].shared = self.options.shared # shared MPFR requires DLL-version of GMP

    def source(self):
        tar_name = f"{self._src_dir}.tar.gz"

        tools.download(f"http://www.mpfr.org/mpfr-{self.version}/{tar_name}",
                       tar_name)

        tools.unzip(tar_name)
        os.unlink(tar_name)

    def _configure_autotools(self):
        env_build = AutoToolsBuildEnvironment(self,
                                              win_bash = tools.os_info.is_windows)

        if self.settings.os != "Windows":
            env_build.fpic = self.options.fPIC

        build_args = [
                         "--prefix=%s" % ((tools.unix_path(self.package_folder) if self.settings.compiler == "gcc" else self.package_folder) if tools.os_info.is_windows else self.package_folder),
                         "--%s-shared" % ("enable" if self.options.shared else "disable"),
                         "--%s-static" % ("enable" if not self.options.shared else "disable")
                     ]

        if self.settings.os != "Windows" and \
           self.settings.arch == "x86":

                env_build.vars["ABI"] = "32"
                env_build.cxx_flags.append("-m32")
                env_build.c_flags.append("-m32")

        # Link GMP:
        env_build.library_paths.append(os.path.join(self.deps_cpp_info['gmp'].rootpath,
                                                    self.deps_cpp_info['gmp'].libdirs[0]))

        env_build.include_paths.append(os.path.join(self.deps_cpp_info['gmp'].rootpath,
                                                    self.deps_cpp_info['gmp'].includedirs[0]))

        # Setup/configure:
        with tools.environment_append(env_build.vars):
            env_build.configure(args = build_args)

        return env_build

    def build(self):
        with tools.chdir(self._src_dir):
            autotools = self._configure_autotools()
            autotools.make()

    def package(self):
        with tools.chdir(self._src_dir):
            autotools = self._configure_autotools()
            autotools.make(args = [
                                      "install"
                                  ])

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        self.cpp_info.libs = tools.collect_libs(self)

        if self.settings.os == "Linux" and \
           self.settings.compiler == "gcc":

            self.cpp_info.libs.append("m")

/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <gmp.h>
#include <mpfr.h>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'MPFR' package test (compilation, linking, and execution).\n";

    mpfr_t a,
           b,
           c;

    std::cout << "'MPFR' package works!" << std::endl;
    return EXIT_SUCCESS;
}

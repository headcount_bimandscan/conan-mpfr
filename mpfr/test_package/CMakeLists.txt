#
# 2018-2019 © BIM & Scan® Ltd.
# See 'README.md' in the project root for more information.
#
cmake_minimum_required(VERSION 3.6 FATAL_ERROR)
project("Conan-MPFR-PkgTest")

include("${CMAKE_BINARY_DIR}/conanbuildinfo.cmake")
conan_basic_setup(TARGETS)

# Build test executable:
add_executable(pkgtest_mpfr "${CMAKE_CURRENT_SOURCE_DIR}/PkgTest.cpp")
target_link_libraries(pkgtest_mpfr PRIVATE CONAN_PKG::mpfr)

# BIM & Scan® Third-Party Library (MPFR)

![BIM & Scan](BimAndScan.png "BIM & Scan® Ltd.")

Conan build script for [MPFR](https://www.cgal.org/), the GNU 'Multiple Precision Arithmetic Library'.

Supports version 4.0.1 (stable).

Requires the [Bincrafters](https://bintray.com/bincrafters/public-conan) Conan repository for third-party dependencies.
